# Translation of plasma_applet_org.kde.plasma.devicenotifier.po to Ukrainian
# Copyright (C) 2013-2020 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2013, 2014, 2016, 2018, 2019, 2020, 2021, 2022, 2023, 2024.
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_org.kde.plasma.devicenotifier\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-23 00:39+0000\n"
"PO-Revision-Date: 2024-01-11 09:42+0200\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 23.04.1\n"

#: package/contents/ui/DeviceItem.qml:188
#, kde-format
msgctxt "@info:status Free disk space"
msgid "%1 free of %2"
msgstr "Вільно %1 з %2"

#: package/contents/ui/DeviceItem.qml:192
#, kde-format
msgctxt ""
"Accessing is a less technical word for Mounting; translation should be short "
"and mean 'Currently mounting this device'"
msgid "Accessing…"
msgstr "Монтування…"

#: package/contents/ui/DeviceItem.qml:195
#, kde-format
msgctxt ""
"Removing is a less technical word for Unmounting; translation should be "
"short and mean 'Currently unmounting this device'"
msgid "Removing…"
msgstr "Видаляємо…"

#: package/contents/ui/DeviceItem.qml:198
#, kde-format
msgid "Don't unplug yet! Files are still being transferred…"
msgstr "Не від'єднуйте! Триває перенесення файлів…"

#: package/contents/ui/DeviceItem.qml:229
#, kde-format
msgid "Open in File Manager"
msgstr "Відкрити у програмі для керування файлами"

#: package/contents/ui/DeviceItem.qml:232
#, kde-format
msgid "Mount and Open"
msgstr "Змонтувати і відкрити"

#: package/contents/ui/DeviceItem.qml:234
#, kde-format
msgid "Eject"
msgstr "Виштовхнути"

#: package/contents/ui/DeviceItem.qml:236
#, kde-format
msgid "Safely remove"
msgstr "Безпечно вилучити"

#: package/contents/ui/DeviceItem.qml:278
#, kde-format
msgid "Mount"
msgstr "Змонтувати"

#: package/contents/ui/FullRepresentation.qml:43
#: package/contents/ui/main.qml:225
#, kde-format
msgid "Remove All"
msgstr "Від'єднати усі"

#: package/contents/ui/FullRepresentation.qml:44
#, kde-format
msgid "Click to safely remove all devices"
msgstr "Натисніть, щоб безпечно від’єднати усі пристрої"

#: package/contents/ui/FullRepresentation.qml:186
#, kde-format
msgid "No removable devices attached"
msgstr "З комп'ютером не з'єднано портативних пристроїв"

#: package/contents/ui/FullRepresentation.qml:186
#, kde-format
msgid "No disks available"
msgstr "Немає доступних дисків"

#: package/contents/ui/main.qml:57
#, kde-format
msgid "Most Recent Device"
msgstr "Останній з’єднаний пристрій"

#: package/contents/ui/main.qml:57
#, kde-format
msgid "No Devices Available"
msgstr "Не виявлено жодного пристрою"

#: package/contents/ui/main.qml:207
#, kde-format
msgctxt "Open auto mounter kcm"
msgid "Configure Removable Devices…"
msgstr "Налаштовування портативних пристроїв…"

#: package/contents/ui/main.qml:233
#, kde-format
msgid "Removable Devices"
msgstr "Портативні пристрої"

#: package/contents/ui/main.qml:248
#, kde-format
msgid "Non Removable Devices"
msgstr "Стаціонарні пристрої"

#: package/contents/ui/main.qml:263
#, kde-format
msgid "All Devices"
msgstr "Усі пристрої"

#: package/contents/ui/main.qml:280
#, kde-format
msgid "Show popup when new device is plugged in"
msgstr "Показувати контекстне меню після з’єднання нового пристрою"

#: plugin/ksolidnotify.cpp:190
#, kde-format
msgid "Device Status"
msgstr "Стан пристрою"

#: plugin/ksolidnotify.cpp:190
#, kde-format
msgid "A device can now be safely removed"
msgstr "Пристрій тепер можна безпечно від’єднувати"

#: plugin/ksolidnotify.cpp:191
#, kde-format
msgid "This device can now be safely removed."
msgstr "Цей пристрій тепер можна безпечно від’єднувати."

#: plugin/ksolidnotify.cpp:198
#, kde-format
msgid "You are not authorized to mount this device."
msgstr "Ви не маєте прав доступу для монтування цього пристрою."

#: plugin/ksolidnotify.cpp:201
#, kde-format
msgctxt "Remove is less technical for unmount"
msgid "You are not authorized to remove this device."
msgstr "Ви не маєте прав доступу для вилучення цього пристрою."

#: plugin/ksolidnotify.cpp:204
#, kde-format
msgid "You are not authorized to eject this disc."
msgstr "Ви не маєте прав доступу до виштовхування цього диска."

#: plugin/ksolidnotify.cpp:211
#, kde-format
msgid "Could not mount this device as it is busy."
msgstr ""
"Не вдалося змонтувати цей пристрій, оскільки його зайнято роботою з іншою "
"програмою."

#: plugin/ksolidnotify.cpp:242
#, kde-format
msgid "One or more files on this device are open within an application."
msgstr "Один або декілька файлів з цього пристрою відкрито у програмі."

#: plugin/ksolidnotify.cpp:244
#, kde-format
msgid "One or more files on this device are opened in application \"%2\"."
msgid_plural ""
"One or more files on this device are opened in following applications: %2."
msgstr[0] ""
"Один або декілька файлів з цього пристрою відкрито у таких програмах: %2."
msgstr[1] ""
"Один або декілька файлів з цього пристрою відкрито у таких програмах: %2."
msgstr[2] ""
"Один або декілька файлів з цього пристрою відкрито у таких програмах: %2."
msgstr[3] ""
"Один або декілька файлів з цього пристрою відкрито у такій програмі: %2."

#: plugin/ksolidnotify.cpp:247
#, kde-format
msgctxt "separator in list of apps blocking device unmount"
msgid ", "
msgstr ", "

#: plugin/ksolidnotify.cpp:265
#, kde-format
msgid "Could not mount this device."
msgstr "Не вдалося змонтувати цей пристрій."

#: plugin/ksolidnotify.cpp:268
#, kde-format
msgctxt "Remove is less technical for unmount"
msgid "Could not remove this device."
msgstr "Не вдалося вилучити цей пристрій."

#: plugin/ksolidnotify.cpp:271
#, kde-format
msgid "Could not eject this disc."
msgstr "Не вдалося виштовхнути цей диск."

#~ msgid "Show:"
#~ msgstr "Показати:"

#~ msgid "General"
#~ msgstr "Загальне"

#~ msgid ""
#~ "It is currently <b>not safe</b> to remove this device: applications may "
#~ "be accessing it. Click the eject button to safely remove this device."
#~ msgstr ""
#~ "Вилучати цей пристрій зараз <b>небезпечно</b>: програми можуть виконувати "
#~ "запис на пристрій. Натисніть кнопку виштовхування, щоб вилучити цей "
#~ "пристрій безпечно."

#~ msgid "This device is currently accessible."
#~ msgstr "Доступ до цього пристрою відкрито."

#~ msgid ""
#~ "It is currently <b>not safe</b> to remove this device: applications may "
#~ "be accessing other volumes on this device. Click the eject button on "
#~ "these other volumes to safely remove this device."
#~ msgstr ""
#~ "Вилучати цей пристрій зараз <b>небезпечно</b>: програми можуть виконувати "
#~ "запис на інші томи цього пристрою. Натисніть кнопку виштовхування інших "
#~ "томів цього пристрою, щоб вилучити цей пристрій безпечно."

#~ msgid "This device is not currently accessible."
#~ msgstr "Доступу до цього пристрою зараз немає."

#~ msgid "1 action for this device"
#~ msgid_plural "%1 actions for this device"
#~ msgstr[0] "%1 дія для цього пристрою"
#~ msgstr[1] "%1 дії для цього пристрою"
#~ msgstr[2] "%1 дій для цього пристрою"
#~ msgstr[3] "%1 дія для цього пристрою"
