# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# SPDX-FileCopyrightText: 2014, 2020, 2023, 2024 A S Alam <aalam@users.sf.net>
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-03-19 00:40+0000\n"
"PO-Revision-Date: 2024-01-01 09:47-0600\n"
"Last-Translator: A S Alam <aalam@punlinux.org>\n"
"Language-Team: Punjabi <kde-i18n-doc@kde.org>\n"
"Language: pa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.08.4\n"

#: baloosearchrunner.cpp:60
#, kde-format
msgid "Open Containing Folder"
msgstr "ਰੱਖਣ ਵਾਲਾ ਫੋਲਡਰ ਖੋਲ੍ਹੋ"

#: baloosearchrunner.cpp:84
#, fuzzy, kde-format
#| msgid "Audios"
msgctxt ""
"Audio files; translate this as a plural noun in languages with a plural form "
"of the word"
msgid "Audio"
msgstr "ਆਡੀਓ"

#: baloosearchrunner.cpp:86
#, kde-format
msgid "Images"
msgstr "ਚਿੱਤਰ"

#: baloosearchrunner.cpp:87
#, kde-format
msgid "Videos"
msgstr "ਵੀਡੀਓ"

#: baloosearchrunner.cpp:88
#, kde-format
msgid "Spreadsheets"
msgstr "ਸਪਰੈਡਸ਼ੀਟ"

#: baloosearchrunner.cpp:89
#, kde-format
msgid "Presentations"
msgstr "ਪੇਸ਼ਕਾਰੀਆਂ"

#: baloosearchrunner.cpp:90
#, kde-format
msgid "Folders"
msgstr "ਫੋਲਡਰ"

#: baloosearchrunner.cpp:91
#, kde-format
msgid "Documents"
msgstr "ਦਸਤਾਵੇਜ਼"

#: baloosearchrunner.cpp:92
#, kde-format
msgid "Archives"
msgstr "ਅਕਾਇਵ"

#: baloosearchrunner.cpp:93
#, kde-format
msgid "Texts"
msgstr "ਲਿਖਤਾਂ"

#: baloosearchrunner.cpp:94
#, kde-format
msgid "Files"
msgstr "ਫਾਇਲਾਂ"

#~ msgid "Search through files, emails and contacts"
#~ msgstr "ਫਾਈਲਾਂ, ਈਮੇਲ ਤੇ ਸੰਪਰਕਾਂ ਵਿੱਚ ਖੋਜ"
