# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Sönke Dibbern <s_dibbern@web.de>, 2014.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-23 00:39+0000\n"
"PO-Revision-Date: 2014-09-18 16:09+0200\n"
"Last-Translator: Sönke Dibbern <s_dibbern@web.de>\n"
"Language-Team: Low Saxon <kde-i18n-nds@kde.org>\n"
"Language: nds\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 1.4\n"

#: contents/ui/BarcodePage.qml:23
#, kde-format
msgid "QR Code"
msgstr ""

#: contents/ui/BarcodePage.qml:24
#, kde-format
msgid "Data Matrix"
msgstr ""

#: contents/ui/BarcodePage.qml:25
#, kde-format
msgctxt "Aztec barcode"
msgid "Aztec"
msgstr ""

#: contents/ui/BarcodePage.qml:26
#, kde-format
msgid "Code 39"
msgstr ""

#: contents/ui/BarcodePage.qml:27
#, kde-format
msgid "Code 93"
msgstr ""

#: contents/ui/BarcodePage.qml:28
#, kde-format
msgid "Code 128"
msgstr ""

#: contents/ui/BarcodePage.qml:44
#, kde-format
msgid "Return to Clipboard"
msgstr ""

#: contents/ui/BarcodePage.qml:79
#, kde-format
msgid "Change the QR code type"
msgstr ""

#: contents/ui/BarcodePage.qml:136
#, kde-format
msgid "Creating QR code failed"
msgstr ""

#: contents/ui/BarcodePage.qml:148
#, kde-format
msgid ""
"There is not enough space to display the QR code. Try resizing this applet."
msgstr ""

#: contents/ui/DelegateToolButtons.qml:26
#, kde-format
msgid "Invoke action"
msgstr "Akschoon opropen "

#: contents/ui/DelegateToolButtons.qml:41
#, fuzzy, kde-format
#| msgid "Show barcode"
msgid "Show QR code"
msgstr "Streekkode wiesen"

#: contents/ui/DelegateToolButtons.qml:57
#, kde-format
msgid "Edit contents"
msgstr "Inholden bewerken"

#: contents/ui/DelegateToolButtons.qml:71
#, kde-format
msgid "Remove from history"
msgstr "Ut Vörgeschicht wegmaken"

#: contents/ui/EditPage.qml:82
#, kde-format
msgctxt "@action:button"
msgid "Save"
msgstr ""

#: contents/ui/EditPage.qml:90
#, kde-format
msgctxt "@action:button"
msgid "Cancel"
msgstr ""

#: contents/ui/main.qml:30
#, kde-format
msgid "Clipboard Contents"
msgstr "Twischenaflaag-Inholt"

#: contents/ui/main.qml:31 contents/ui/Menu.qml:134
#, kde-format
msgid "Clipboard is empty"
msgstr "Twischenaflaag is leddig"

#: contents/ui/main.qml:59
#, fuzzy, kde-format
#| msgid "Clear history"
msgid "Clear History"
msgstr "Vörgeschicht leddig maken"

#: contents/ui/main.qml:71
#, kde-format
msgid "Configure Clipboard…"
msgstr ""

#: contents/ui/Menu.qml:134
#, kde-format
msgid "No matches"
msgstr ""

#: contents/ui/UrlItemDelegate.qml:107
#, kde-format
msgctxt ""
"Indicator that there are more urls in the clipboard than previews shown"
msgid "+%1"
msgstr "+%1"

#~ msgid "Clear history"
#~ msgstr "Vörgeschicht leddig maken"

#, fuzzy
#~| msgid "Search"
#~ msgid "Search…"
#~ msgstr "Söken"

#, fuzzy
#~| msgid "Clipboard is empty"
#~ msgid "Clipboard history is empty."
#~ msgstr "Twischenaflaag is leddig"

#~ msgid "Configure"
#~ msgstr "Instellen"
